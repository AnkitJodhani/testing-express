const express = require('express');
const morgan = require('morgan');



const app = express();

app.use(morgan('common'));
app.use(express.static(__dirname + 'public'));
app.get('/',(req,res,next)=>{
    res.send(`<h1>HOME PAGE<h1>`)
})


app.get('/profile',(req,res,next)=>{
    res.status(200).json({
        message:"Profile page"
    })
})

app.get('/aboutus',(req,res,next)=>{
    res.status(200).json({
        message:"About us page"
    })
})

app.get('/services',(req,res,next)=>{
    res.status(200).json({
        message:"Services page"
    })
})

const PORT = 5000;
app.listen(PORT,()=>{
    console.log(`Server listning on port ${PORT} `);
})